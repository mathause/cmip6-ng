#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2020-03-05 14:36:55 lukbrunn>

(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Ruth Lorenz || ruth.lorenz@env.ethz.ch
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import os
import cdo
import logging
import subprocess

logger = logging.getLogger(__name__)

CDO = cdo.Cdo()


def _yearmonmean_cdo(load_filename, save_filename):
    CDO.yearmonmean(
        options='-b F64',
        input=load_filename,
        output=save_filename)


def yearly_mean_cdo(filename, overwrite=False, cdo_version='1.9.6'):
    if isinstance(cdo_version, str):
        cdo_version = [cdo_version]
    if cdo.getCdoVersion('cdo') not in cdo_version:
        errmsg = f'cdo: {cdo.getCdoVersion("cdo")} not in {cdo_version}'
        logger.error(errmsg)
        raise ValueError(errmsg)

    save_path = os.path.dirname(filename).replace('/mon', f'/ann')
    os.makedirs(save_path, exist_ok=True)
    save_filename = os.path.basename(filename).replace(
        '_mon', f'_ann')
    save_filename = os.path.join(save_path, save_filename)

    if os.path.isfile(save_filename) and not overwrite:
        logger.debug(f'Skip existing file:\n  {save_filename}')
        return None
    elif os.path.isfile(save_filename) and overwrite:
        os.remove(save_filename)  # delete old file to avoid permission errors
    else:
        NotImplementedError

    logger.info(f'Process target file:\n  {save_filename}')
    try:
        _yearmonmean_cdo(filename, save_filename)
    except cdo.CDOException:
        os.remove(save_filename)  # make sure no broken file is left
        logger.error(f'cdo error! Annual mean calculation failed for {save_filename}')
        return None

    # subprocess.run([
    #     '/usr/local/bin/ncatted', '-a',
    #     f'cmip6-ng,global,a,c,"\ntime_average = yearmonmean (see CDO)"',
    #     save_filename])

    logger.info(f'Saved file:\n  {save_filename}')

    return save_filename
