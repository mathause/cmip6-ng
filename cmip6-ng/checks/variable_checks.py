#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2021-04-08 15:36:01 lukas>

(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Ruth Lorenz || ruth.lorenz@env.ethz.ch
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: Variable dependent sanity checks. Mainly checking the full valid
range as well as the valid range of global (not-area weighted!) mean.

WARNING
-------
Please NEVER EVER do any assignments to the 'ds' object here!!
(The checks functions should never change the dataset in any way)

TODO
----
- Several variables miss sensible ranges to check
"""
import logging

from configs.variable_ranges import WARNING_RANGE, ERROR_RANGE

logger = logging.getLogger(__name__)

DIMENSIONS_1D = ('time')
DIMENSIONS_2D = ('lat', 'lon')
DIMENSIONS_3D = ('time', 'lat', 'lon')
DIMENSIONS_3D_2 = ('time', 'y', 'x')
DIMENSIONS_4D = ('time', 'depth', 'lat', 'lon')
DIMENSIONS_4D_2 = ('time', 'plev', 'lat', 'lon')


def check_range_full(da, varn, warning=False):
    min_ = float(da.min().data)  # force value from dask
    max_ = float(da.max().data)
    unit = da.attrs['units']  # for logging only
    if warning:
        min_ref = WARNING_RANGE[varn][0]
        max_ref = WARNING_RANGE[varn][1]
        if min_ < min_ref or max_ > max_ref:
            logger.debug(f'{varn} ({unit}): {min_:.6f} < {min_ref} or {max_:.6f} > {max_ref}')
            return ' '.join([
                f'(min, max) of variable outside warning range: ({min_ref},',
                f'{max_ref})'])
        return True
    else:
        min_ref = ERROR_RANGE[varn][0]
        max_ref = ERROR_RANGE[varn][1]
        if min_ < min_ref or max_ > max_ref:
            logger.error(f'{varn} ({unit}): {min_:.4f} < {min_ref} or {max_:.4f} > {max_ref}')
            return False
        return True


def check_unit(da, unit_ref):
    unit = da.attrs['units']
    if unit != unit_ref:
        logger.error(f'{unit} != {unit_ref}')
        return False
    return True


def check_pr(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'kg m-2 s-1'):
        return False
    if not check_range_full(da, 'pr'):
        return False
    return check_range_full(da, 'pr', warning=True)


def check_psl(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'Pa'):
        return False
    if not check_range_full(da, 'psl'):
        return False
    return check_range_full(da, 'psl', warning=True)


def check_siconc(da):
    # see check_tos
    # if da.dims != DIMENSIONS_3D_2:
    #     raise VariableChecksError(f'{da.dims} != {DIMENSIONS_3D_2}')
    if not check_unit(da, '%'):
        return False
    if not check_range_full(da, 'siconc'):
        return False
    return check_range_full(da, 'siconc', warning=True)


def check_tas(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'K'):
        return False
    if not check_range_full(da, 'tas'):
        return False
    return check_range_full(da, 'tas', warning=True)


def check_tasmin(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'K'):
        return False
    if not check_range_full(da, 'tasmin'):
        return False
    return check_range_full(da, 'tasmin', warning=True)


def check_tasmax(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'K'):
        return False
    if not check_range_full(da, 'tasmax'):
        return False
    return check_range_full(da, 'tasmax', warning=True)


def check_tos(da):
    # NOTE: tos has several different dimension names, e.g., (lat, lon),
    # (x, y), (j, i), (nj, ni), ... At the moment they are left as they are and the
    # dimension name check is disabled.
    # if da.dims != DIMENSIONS_3D_2:
    #     raise VariableChecksError(f'{da.dims} != {DIMENSIONS_3D_2}')
    if not check_unit(da, 'degC'):
        return False
    if not check_range_full(da, 'tos'):
        return False
    return check_range_full(da, 'tos', warning=True)


def check_clt(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, '%'):
        return False
    if da.max() <= 5.:
        logger.error('clt (%) max < 5. (fraction instead of % ?)')
        return False
    if not check_range_full(da, 'clt'):
        return False
    return check_range_full(da, 'clt', warning=True)


def check_hurs(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, '%'):
        return False
    if da.max() <= 5.:
        logger.error('hurs (%) max < 5. (fraction instead of % ?)')
        return False
    if not check_range_full(da, 'hurs'):
        return False
    return check_range_full(da, 'hurs', warning=True)


def check_huss(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, '1'):
        return False
    if not check_range_full(da, 'huss'):
        return False
    return check_range_full(da, 'huss', warning=True)


def check_rlut(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rlut'):
        return False
    return check_range_full(da, 'rlut', warning=True)


def check_rsdt(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rsdt'):
        return False
    return check_range_full(da, 'rsdt', warning=True)


def check_rsut(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rsut'):
        return False
    return check_range_full(da, 'rsut', warning=True)


def check_rtmt(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rtmt'):
        return False
    return check_range_full(da, 'rtmt', warning=True)


def check_rlds(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rlds'):
        return False
    return check_range_full(da, 'rlds', warning=True)


def check_rlus(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rlus'):
        return False
    return check_range_full(da, 'rlus', warning=True)


def check_rsds(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rsds'):
        return False
    return check_range_full(da, 'rsds', warning=True)


def check_rsus(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'rsus'):
        return False
    return check_range_full(da, 'rsus', warning=True)


def check_hfss(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'hfss'):
        return False
    return check_range_full(da, 'hfss', warning=True)


def check_hfls(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'hfls'):
        return False
    return check_range_full(da, 'hfls', warning=True)


def check_prw(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'kg m-2'):
        return False
    if not check_range_full(da, 'prw'):
        return False
    return check_range_full(da, 'prw', warning=True)


def check_tau(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'Pa'):
        return False
    if not check_range_full(da, 'tauu'):
        return False
    return check_range_full(da, 'tauu', warning=True)


def check_sftlf(da):
    if da.dims != DIMENSIONS_2D:
        logger.error(f'{da.dims} != {DIMENSIONS_2D}')
        return False
    if not check_unit(da, '%'):
        return False
    if not check_range_full(da, 'sftlf'):
        return False
    return check_range_full(da, 'sftlf', warning=True)


def check_areacella(da):
    if da.dims != DIMENSIONS_2D:
        logger.error(f'{da.dims} != {DIMENSIONS_2D}')
        return False
    if not check_unit(da, 'm2'):
        return False
    if not check_range_full(da, 'areacella'):
        return False
    return check_range_full(da, 'areacella', warning=True)


def check_evspsbl(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'kg m-2 s-1'):
        return False
    if not check_range_full(da, 'evspsbl'):
        return False
    return check_range_full(da, 'evspsbl', warning=True)


def check_zg500(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'm'):
        return False
    if not check_range_full(da, 'zg500'):
        return False
    return check_range_full(da, 'zg500', warning=True)


def check_gpp(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'kg m-2 s-1'):
        return False
    if not check_range_full(da, 'gpp'):
        return False
    return check_range_full(da, 'gpp', warning=True)


def check_ra(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'kg m-2 s-1'):
        return False
    if not check_range_full(da, 'ra'):
        return False
    return check_range_full(da, 'ra', warning=True)


def check_co2mass(da):
    if da.dims != DIMENSIONS_1D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'kg'):
        return False
    if not check_range_full(da, 'co2mass'):
        return False
    return check_range_full(da, 'co2mass', warning=True)


def check_mrso(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'kg m-2'):
        return False
    if not check_range_full(da, 'mrso'):
        return False
    return check_range_full(da, 'mrso', warning=True)


def check_lai(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, '1'):
        return False
    if not check_range_full(da, 'lai'):
        return False
    return check_range_full(da, 'lai', warning=True)


def check_mrro(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, 'kg m-2 s-1'):
        return False
    if not check_range_full(da, 'mrro'):
        return False
    return check_range_full(da, 'mrro', warning=True)


def check_tsl(da):
    if da.dims != DIMENSIONS_4D:
        logger.error(f'{da.dims} != {DIMENSIONS_4D}')
        return False
    if not check_unit(da, 'K'):
        return False
    if not check_range_full(da, 'tsl'):
        return False
    return check_range_full(da, 'tsl', warning=True)


def check_mrsol(da):
    if da.dims != DIMENSIONS_4D:
        logger.error(f'{da.dims} != {DIMENSIONS_4D}')
        return False
    if not check_unit(da, 'kg m-2'):
        return False
    if not check_range_full(da, 'mrsol'):
        return False
    return check_range_full(da, 'mrsol', warning=True)


def check_ta(da):
    if da.dims != DIMENSIONS_4D_2:
        logger.error(f'{da.dims} != {DIMENSIONS_4D_2}')
        return False
    if not check_unit(da, 'K'):
        return False
    if not check_range_full(da, 'ta'):
        return False
    return check_range_full(da, 'ta', warning=True)


def check_treeFrac(da):
    if da.dims != DIMENSIONS_3D:
        logger.error(f'{da.dims} != {DIMENSIONS_3D}')
        return False
    if not check_unit(da, '%'):
        return False
    if da.max() <= 5.:
        logger.error('clt (%) max < 5. (fraction instead of % ?)')
        return False
    if not check_range_full(da, 'treeFrac'):
        return False
    return check_range_full(da, 'treeFrac', warning=True)


def check_hfds(da):
    # if da.dims != DIMENSIONS_3D:
    #     logger.error(f'{da.dims} != {DIMENSIONS_3D}')
    #     return False
    if not check_unit(da, 'W m-2'):
        return False
    if not check_range_full(da, 'hfds'):
        return False
    return check_range_full(da, 'hfds', warning=True)


def variable_checks(ds, varn):
    if varn in ['pr']:
        return check_pr(ds[varn])
    elif varn in ['psl']:
        return check_psl(ds[varn])
    elif varn in ['siconc', 'siconca']:
        return check_siconc(ds[varn])
    elif varn in ['tas']:
        return check_tas(ds[varn])
    elif varn in ['tasmax']:
        return check_tasmax(ds[varn])
    elif varn in ['tasmin']:
        return check_tasmin(ds[varn])
    elif varn in ['ta']:
        return check_ta(ds[varn])
    elif varn in ['tos']:
        return check_tos(ds[varn])
    elif varn in ['tsl']:
        return check_tsl(ds[varn])
    elif varn in ['clt']:
        return check_clt(ds[varn])
    elif varn in ['hurs']:
        return check_hurs(ds[varn])
    elif varn in ['huss']:
        return check_huss(ds[varn])
    elif varn in ['rlut', 'rlutcs']:
        return check_rlut(ds[varn])
    elif varn in ['rsdt']:
        return check_rsdt(ds[varn])
    elif varn in ['rsut', 'rsutcs']:
        return check_rsut(ds[varn])
    elif varn in ['rsds', 'rsdscs']:
        return check_rsds(ds[varn])
    elif varn in ['rsus', 'rsuscs']:
        return check_rsus(ds[varn])
    elif varn in ['rlds', 'rldscs']:
        return check_rlds(ds[varn])
    elif varn in ['rlus']:
        return check_rlus(ds[varn])
    elif varn in ['rtmt']:
        return check_rtmt(ds[varn])
    elif varn in ['hfls']:
        return check_hfls(ds[varn])
    elif varn in ['hfss']:
        return check_hfss(ds[varn])
    elif varn in ['hfds']:
        return check_hfds(ds[varn])
    elif varn in ['prw']:
        return check_prw(ds[varn])
    elif varn in ['tauu', 'tauv']:
        return check_tau(ds[varn])
    elif varn in ['sftlf']:
        return check_sftlf(ds[varn])
    elif varn in ['areacella']:
        return check_areacella(ds[varn])
    elif varn in ['evspsbl', 'tran', 'evspsblveg', 'evspsblsoi']:
        return check_evspsbl(ds[varn])
    elif varn in ['zg500']:
        return check_zg500(ds[varn])
    elif varn in ['gpp', 'npp', 'nbp']:
        return check_gpp(ds[varn])
    elif varn in ['ra', 'rh']:
        return check_ra(ds[varn])
    elif varn in ['co2mass']:
        return check_co2mass(ds[varn])
    elif varn in ['lai']:
        return check_lai(ds[varn])
    elif varn in ['mrso', 'mrsos']:
        return check_mrso(ds[varn])
    elif varn in ['mrro', 'mrros']:
        return check_mrro(ds[varn])
    elif varn in ['mrsol']:
        return check_mrsol(ds[varn])
    elif varn in ['x',
                  'y']:
        pass  # TODO
    elif varn in ['treeFrac']:
        return check_treeFrac(ds[varn])
    else:
        raise NotImplementedError(f'{varn}')
