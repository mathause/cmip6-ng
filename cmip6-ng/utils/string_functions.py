#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Time-stamp: <2021-04-06 09:15:08 lukas>

(c) 2019 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract:

"""
import os
import glob
import time
import logging

logger = logging.getLogger(__name__)

LOAD_PATH = '/net/atmos/data/cmip6'
SAVE_PATH = '/net/ch4/data/cmip6-Next_Generation'


def select_grid(path, valid_grids=('gn', 'gr', 'gr1', 'gm')):
    """
    Add grid folder to the filepath if found in valid_grids.

    Parameters
    ----------
    path : string
        Path until the grid folder depth.
    valid_grids : tuple of strings
        Grids to search for. First matching one will be selected!!
        See additional information on grid labels below.

    Returns
    -------
    path : string
        path with grid folder added

    Information
    -----------
    A list of all valid grid labels:
    https://github.com/WCRP-CMIP/CMIP6_CVs/blob/master/CMIP6_grid_label.json

    Additional information:
    https://docs.google.com/document/d/1h0r8RZr_f3-8egBMMh7aqLwy3snpD6_MrDz1q8n5XUk/edit#
    https://docs.google.com/document/d/1kZw3KXvhRAJdBrXHhXo4f6PDl_NzrFre1UfWGHISPz4/edit
    """
    # This can happen due to a network reset by Urs, which should be done in <5s
    if not os.path.exists(path):
        time.sleep(5)  # wait 5 seconds

    # DEBUG: If it is still not there it is something else
    if not os.path.exists(path):
        errmsg = f'{path} does not exist!'
        logger.error(errmsg)
        return None

    for grid in valid_grids:
        if grid in os.listdir(path):
            return os.path.join(path, grid)
    return None


def get_files(run_type, time_agg, varn, base_path=LOAD_PATH):
    """
    Get all files for the given parameters.

    Parameters
    ----------
    run_type : string
        Valid run type (e.g., historical, ssp126, ...)
    time_agg : string
        Valid time aggregation (e.g., Amon, day, ...)
    varn : string
        Valid variable name (e.g., tas, pr, ...)
    base_path : string, optional
        Base path to CMIP6 raw files

    Returns
    -------
    path : generator of string
        Generator object of all files in the lowest (i.e., grid) directory.
    """
    if not os.path.isdir(os.path.join(base_path, run_type, time_agg, varn)):
        logmsg = f'Variable {varn} not found for {run_type}, {time_agg}'
        logger.info(logmsg)
        return []

    paths = glob.glob(os.path.join(
        base_path, run_type, time_agg, varn, '*', '*'))

    if len(paths) == 0:
        logmsg = f'No files found for: {run_type}, {time_agg}, {varn}'
        logger.info(logmsg)
        return []

    for path in paths:
        path_grid = select_grid(path)
        if path_grid is None:
            errmsg = f'No valid grid found for {path}'
            logger.error(errmsg)
            continue
        files = sorted(glob.glob(os.path.join(path_grid, '*.nc')))
        if len(files) == 0:
            errmsg = f'No files found for {path_grid}'
            logger.error(errmsg)
            continue
        yield [os.path.join(path_grid, fn) for fn in files]


def get_files_all(run_types, varns, var_type):
    """
    Return filename lists for the monthly target resolution.

    Parameters
    ----------
    run_types : list of strings
    varns : list of strings
    var_type : {'Amon', Omon', 'SImon', 'day', 'SIday'}

    Returns
    -------
    files : list of strings
    """
    for run_type in run_types:
        for varn in varns:
            for files in get_files(run_type, var_type, varn):
                if files is None:
                    continue
                yield files


def user_paths(paths):
    for path in paths:
        files = sorted(glob.glob(os.path.join(path, '*.nc')))
        yield [os.path.join(path, fn) for fn in files]


def split_path(path):
    """
    Extract meta-information from file path.

    Parameters
    ----------
    path : string
        Full path and filename of a single file.

    Returns
    -------
    parts : dictionary
    """
    path = path.replace(os.path.join(LOAD_PATH, ''), '')
    parts = path.split('/')

    if len(parts) != 7:
        errmsg = f'Unexpected number of sub-folders in path (!=7): {parts}'
        raise ValueError(errmsg)

    return {
        'run_type': parts[0],
        'time_agg': parts[1],
        'varn': parts[2],
        'model': parts[3],
        'ensemble': parts[4],
        'grid': parts[5]}


def split_filename(path, nparts=7):
    """
    Extract meta-information from filename.

    Parameters
    ----------
    path : string
        Full path and filename of a single file.
    nparts : int, optional
        Number of '_' separated parts in the filename (default=7).

    Returns
    -------
    parts : dictionary
    """
    fn = os.path.splitext(os.path.basename(path))[0]
    parts = fn.split('_')

    # fix a bug in the filename (syear_eyear instead of syear-eyear)
    if nparts == 7 and len(parts) == 8 and parts[2] == 'SAM0-UNICON':
        parts[6] = f'{parts[6]}-{parts[7]}'
        parts.pop()

    if len(parts) != nparts:
        errmsg = f'Unexpected number of filename parts (!={nparts}): {parts}'
        raise ValueError(errmsg)

    dd = {
        'run_type': parts[3],
        'time_agg': parts[1],
        'varn': parts[0],
        'model': parts[2],
        'ensemble': parts[4],
        'grid': parts[5],
    }

    if nparts == 7:
        dd['time'] = parts[6]

    return dd


def get_ng_path(varn, time_res):
    """
    Build the path to the ng-archive.

    Parameters
    ----------
    varn : string
    time_res : string

    Returns
    -------
    path : string
    """
    return os.path.join(SAVE_PATH, varn, time_res, 'native')


def get_ng_filename(varn, time_res, model, run_type, ensemble):
    """
    Build the filename for the ng-archive.

    Parameters
    ----------
    varn : string
    time_rest : strings
    model : string
    run_type : string
    ensemble : string

    Returns
    -------
    filename : string
    """
    return f'{varn}_{time_res}_{model}_{run_type}_{ensemble}_native.nc'


def check_filename_vs_path(paths, nparts=7):
    """
    Check if meta-information from path and filename matches.

    Parameters
    ----------
    paths : list of strings
    nparts : int, optional
        Number of '_' separated parts in the filename (default=7).

    Return
    ------
    metadata : dictionary
    """
    for idx, path in enumerate(paths):
        filename_parts = split_filename(path, nparts)
        path_parts = split_path(path)

        # NOTE: NorESM2-LM has run_type and model interchanged in the filename!
        if filename_parts['run_type'] == 'NorESM2-LM':
            filename_parts['run_type'] = filename_parts['model']
            filename_parts['model'] = 'NorESM2-LM'

        for key in path_parts.keys():
            if filename_parts[key] != path_parts[key]:
                errmsg = ' '.join([
                    f'Path does not match filename information for {path}:',
                    f'{path_parts[key]} != {filename_parts[key]}'])
                raise ValueError(errmsg)

        if 'time' in filename_parts.keys():
            if idx == 0:
                starttime = filename_parts['time'].split('-')[0]
            if idx == len(paths) - 1:
                endtime = filename_parts['time'].split('-')[1]
            filename_parts.pop('time')
        else:
            starttime = None
            endtime = None

    return filename_parts, (starttime, endtime)
