#!/bin/bash

. /etc/profile.d/modules.sh

module load conda/2019
source activate iacpy_cmip6_ng
cd /home/rlorenz/scripts/cmip6-ng/cmip6-ng/
nice python cmip6_ng.py --log-level 40
